package com.books.app.domain.model

data class SortedBooksModel(
    val books: List<BookModel>
)