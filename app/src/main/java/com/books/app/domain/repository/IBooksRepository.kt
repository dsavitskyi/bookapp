package com.books.app.domain.repository

import com.books.app.domain.model.BookModel
import com.books.app.domain.model.BooksWithBanners
import com.google.firebase.remoteconfig.FirebaseRemoteConfig

interface IBooksRepository {

    fun activateConfigs(): FirebaseRemoteConfig

    suspend fun getAllBooks(): BooksWithBanners?

    suspend fun getBooksDetailsCarousel(): List<BookModel>

    suspend fun getSections(): List<Int>
}