package com.books.app.domain.extentions


/**
 * Executes the [call] block and returns the result of call or null if any exception occurred
 * */
inline fun <T : Any?> Any.executeSafeOrNull(
    call: () -> T
): T? {
    return try {
        call()
    } catch (e: Throwable) {
        loge(e)
        null
    }
}
