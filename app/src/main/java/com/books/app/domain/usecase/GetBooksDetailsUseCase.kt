package com.books.app.domain.usecase

import com.books.app.domain.model.BookModel
import com.books.app.domain.model.BooksRecommendation
import com.books.app.domain.repository.IBooksRepository
import javax.inject.Inject

class GetBooksDetailsUseCase @Inject constructor(
    private val booksRepository: IBooksRepository
) : EmptyParamsUseCase<BooksRecommendation> {

    override suspend fun execute(params: UseCase.EmptyParams): BooksRecommendation {
        val booksDetails = booksRepository.getBooksDetailsCarousel()
        val sections = booksRepository.getSections()
        val recommendation = mutableListOf<BookModel>()

        sections.forEach { bookId ->
            booksDetails.forEach {
                if (bookId == it.id) recommendation.add(it)
            }
        }

        return BooksRecommendation(
            headerBooks = booksDetails,
            recommendBooks = recommendation
        )
    }
}