package com.books.app.domain.usecase

import com.books.app.domain.model.SortedBooksModel
import com.books.app.domain.model.SortedBooksWithBanners
import com.books.app.domain.repository.IBooksRepository
import javax.inject.Inject

class GetBooksUseCase @Inject constructor(
    private val booksRepository: IBooksRepository
) : EmptyParamsUseCase<SortedBooksWithBanners?> {

    override suspend fun execute(params: UseCase.EmptyParams): SortedBooksWithBanners? {
        var sortedBooks: SortedBooksWithBanners? = null

        booksRepository.getAllBooks()?.let {

            val sortedByGenre = it.books.sortedBy { it.genre }

            val fantasyBook = SortedBooksModel(it.books.filter { it.genre == Genre.FANTASY.value })
            val scienceBook = SortedBooksModel(it.books.filter { it.genre == Genre.SCIENCE.value })
            val romanceBook = SortedBooksModel(it.books.filter { it.genre == Genre.ROMANCE.value })

            val genreBooks = mutableListOf<SortedBooksModel>(fantasyBook, scienceBook, romanceBook)

            sortedBooks = SortedBooksWithBanners(
                banners = it.banners,
                books = genreBooks
            )
        }
        return sortedBooks
    }

    enum class Genre(val value: String) {
        FANTASY("Fantasy"),
        SCIENCE("Science"),
        ROMANCE("Romance")
    }
}