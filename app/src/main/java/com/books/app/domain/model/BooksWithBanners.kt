package com.books.app.domain.model

data class BooksWithBanners(
    val books: List<BookModel>,
    val banners: List<BannerModel>
)
