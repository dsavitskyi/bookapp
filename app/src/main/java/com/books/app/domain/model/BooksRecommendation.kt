package com.books.app.domain.model

data class BooksRecommendation(
    val headerBooks: List<BookModel>,
    val recommendBooks: List<BookModel>
)