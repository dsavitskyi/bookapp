package com.books.app.domain.model


data class BannerModel(
    val bookId: Int,
    val cover: String,
    val id: Int
)