package com.books.app.domain.model

data class SortedBooksWithBanners(
    val banners: List<BannerModel>,
    val books: List<SortedBooksModel>
)