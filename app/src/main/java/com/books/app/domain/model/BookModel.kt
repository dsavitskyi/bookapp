package com.books.app.domain.model


data class BookModel(
    val author: String,
    val coverUrl: String,
    val genre: String,
    val id: Int,
    val likes: String,
    val name: String,
    val quotes: String,
    val summary: String,
    val views: String
)