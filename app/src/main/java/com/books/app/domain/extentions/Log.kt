package com.books.app.domain.extentions

import android.util.Log
import com.books.app.BuildConfig


fun loge(tag: String, message: String, force: Boolean) {
    if (force || BuildConfig.DEBUG) {
        Log.e(tag, message)
    }
}

fun Any.loge(ex: Throwable, force: Boolean = false) {
    if (BuildConfig.DEBUG)
        loge(this::class.java.name, "${ex}\n${ex.stackTraceToString()}", force)
}




