package com.books.app.data.datasource.remote

import com.books.app.R
import com.books.app.data.datasource.model.Book
import com.books.app.data.datasource.model.BooksResponse
import com.google.firebase.ktx.Firebase
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.ktx.get
import com.google.firebase.remoteconfig.ktx.remoteConfig
import com.google.firebase.remoteconfig.ktx.remoteConfigSettings
import com.google.gson.Gson
import javax.inject.Inject

class BooksRemoteDataSource @Inject constructor() {

    private val remoteConfig: FirebaseRemoteConfig = Firebase.remoteConfig

    init {
        initRemoteConfigs()
    }

    private fun initRemoteConfigs() {
        val configSettings = remoteConfigSettings {
            minimumFetchIntervalInSeconds = 3600
        }
        remoteConfig.setConfigSettingsAsync(configSettings)
        remoteConfig.setDefaultsAsync(R.xml.remote_config_defaults)
    }

    fun getRemoteConfig(): FirebaseRemoteConfig = remoteConfig

    fun getAllBooks(): BooksResponse {
        return parseBooks()
    }

    fun getSections(): List<Int> {
        return parseBooks().youWillLikeSection
    }

    fun getBooksDetailsCarousel(): List<Book> {
        val booksJson = remoteConfig.get(RemoteConfigValue.BOOKS_DETAILS_CAROUSEL.value).asString()
        val gson = Gson()
        val jsonModel = gson.fromJson(booksJson, BooksResponse::class.java)
        return jsonModel.books
    }

    private fun parseBooks(): BooksResponse {
        val booksJson = remoteConfig.get(RemoteConfigValue.BOOKS.value).asString()
        val gson = Gson()
        return gson.fromJson(booksJson, BooksResponse::class.java)
    }

    enum class RemoteConfigValue(val value: String) {
        BOOKS("json_data"),
        BOOKS_DETAILS_CAROUSEL("details_carousel")
    }
}