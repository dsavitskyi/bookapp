package com.books.app.data.datasource.model

import com.google.gson.annotations.SerializedName

data class BooksResponse(
    @SerializedName("books") val books: List<Book>,
    @SerializedName("top_banner_slides") val topBannerSlides: List<TopBannerSlide>,
    @SerializedName("you_will_like_section") val youWillLikeSection: List<Int>
)