package com.books.app.data.mapper

import com.books.app.data.datasource.model.Book
import com.books.app.data.datasource.model.BooksResponse
import com.books.app.data.datasource.model.TopBannerSlide
import com.books.app.domain.model.BannerModel
import com.books.app.domain.model.BookModel
import com.books.app.domain.model.BooksWithBanners

fun Book.toDomain() = BookModel(
    author = author,
    coverUrl = coverUrl,
    genre = genre,
    id = id,
    likes = likes,
    name = name,
    quotes = quotes,
    summary = summary,
    views = views
)

fun TopBannerSlide.toDomain() = BannerModel(
    bookId = bookId,
    cover = cover,
    id = id
)

fun BooksResponse.toDomain() = BooksWithBanners(
    books = books.map { it.toDomain() }?: emptyList(),
    banners = topBannerSlides.map { it.toDomain() }?: emptyList()
)

fun List<Book>.toDomain() = this.map { it.toDomain() }