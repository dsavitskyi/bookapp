package com.books.app.data.repository

import com.books.app.data.datasource.remote.BooksRemoteDataSource
import com.books.app.data.mapper.toDomain
import com.books.app.domain.extentions.executeSafeOrNull
import com.books.app.domain.model.BookModel
import com.books.app.domain.model.BooksWithBanners
import com.books.app.domain.repository.IBooksRepository
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import javax.inject.Inject

class BooksRepository @Inject constructor(
    private val booksRemoteDataSource: BooksRemoteDataSource
): IBooksRepository {

    override fun activateConfigs(): FirebaseRemoteConfig {
        return booksRemoteDataSource.getRemoteConfig()
    }

    override suspend fun getAllBooks(): BooksWithBanners? {
        return executeSafeOrNull { booksRemoteDataSource.getAllBooks().toDomain() }
    }

    override suspend fun getBooksDetailsCarousel(): List<BookModel> {
        return executeSafeOrNull { booksRemoteDataSource.getBooksDetailsCarousel().toDomain() }?: emptyList()
    }

    override suspend fun getSections(): List<Int> {
        return executeSafeOrNull { booksRemoteDataSource.getSections() }?: emptyList()
    }
}