package com.books.app.presentation.screens.details

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.books.app.domain.model.BookModel
import com.books.app.domain.usecase.GetBooksDetailsUseCase
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.shareIn
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

class BookDetailsViewModel @Inject constructor(
    private val getBooksDetailsUseCase: GetBooksDetailsUseCase
) : ViewModel() {

    private val _scrollFlow = MutableStateFlow<Int>(0)
    val scrollFlow: StateFlow<Int> = _scrollFlow

    private val _currentBookFlow = MutableSharedFlow<BookModel>()
    val currentBookFlow: SharedFlow<BookModel> = _currentBookFlow

    val booksFlow = flow {
        val books = getBooksDetailsUseCase.execute()
        emit(books)
    }.shareIn(viewModelScope, SharingStarted.WhileSubscribed(), replay = 1)


    fun setSelectedBook(position: Int) {
        val books = booksFlow.replayCache.get(0).headerBooks
        val book = books.get(position)
        viewModelScope.launch {
            _currentBookFlow.emit(book)
        }
    }

    fun scrollToPosition(bookId: Int) {
        val list = booksFlow.replayCache.get(0).headerBooks
        val books = list.find { it.id == bookId }
        val index = list.indexOf(books)
        _scrollFlow.update { (index) }
    }
}