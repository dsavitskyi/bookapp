package com.books.app.presentation.screens.welcome

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.stateIn
import javax.inject.Inject

class WelcomeViewModel @Inject constructor() : ViewModel() {

    val navigationFlow = flow {
        delay(PROGRESS_SHOW_DELAY)
        emit(true)
    }.stateIn(viewModelScope, SharingStarted.WhileSubscribed(), false)

    companion object {
        const val PROGRESS_SHOW_DELAY = 2000L
    }
}