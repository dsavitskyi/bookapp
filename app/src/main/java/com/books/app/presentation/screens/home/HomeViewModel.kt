package com.books.app.presentation.screens.home

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.books.app.domain.usecase.GetBooksUseCase
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.shareIn
import kotlinx.coroutines.launch
import javax.inject.Inject

class HomeViewModel @Inject constructor(
    private val getBooksUseCase: GetBooksUseCase
) : ViewModel() {

    private val _updateBannerFlow = MutableSharedFlow<Int>()
    val updateBannerFlow: SharedFlow<Int> = _updateBannerFlow

    private var bannerJob: Job? = null

    val booksFlow = flow {
        getBooksUseCase.execute()?.let {
            emit(it)
        }
    }.shareIn(viewModelScope, SharingStarted.WhileSubscribed())

    fun prepareBannerAutoScroll(bannerSize: Int) {
        if (bannerJob == null) {
            bannerJob = viewModelScope.launch {
                delay(PREPARE_AUTO_SLIDE_DELAY)
                var i = 0
                do {
                    if (i == bannerSize) i = 0
                    delay(AUTO_SLIDE_DELAY)
                    _updateBannerFlow.emit(i)
                    i++
                } while (true)
            }
        }
    }

    fun cancelBannerJob() {
        if (bannerJob != null) {
            bannerJob!!.cancel()
            bannerJob = null
        }
    }

    companion object {
        const val PREPARE_AUTO_SLIDE_DELAY: Long = 3000
        const val AUTO_SLIDE_DELAY: Long = 1000
    }
}