package com.books.app.presentation.screens.details

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.RecyclerView
import com.books.app.R
import com.books.app.databinding.FragmentBookDetailsBinding
import com.books.app.domain.extentions.collectWithLifecycle
import com.books.app.domain.extentions.getSnapPosition
import com.books.app.domain.model.BookModel
import com.books.app.presentation.ViewBindingFragment
import com.books.app.presentation.screens.details.bookslikeadapter.BooksLikeAdapter
import com.books.app.presentation.screens.home.booksdetailadapter.BooksDetailAdapter
import com.books.app.presentation.screens.home.booksdetailadapter.BoundsOffsetDecoration
import com.books.app.presentation.screens.home.booksdetailadapter.LinearHorizontalSpacingDecoration
import com.books.app.presentation.screens.home.booksdetailadapter.ProminentLayoutManager
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import javax.inject.Inject

class BookDetailsFragment: ViewBindingFragment<FragmentBookDetailsBinding>(FragmentBookDetailsBinding::inflate) {

    @Inject
    lateinit var factory: ViewModelProvider.Factory
    val viewModel: BookDetailsViewModel by viewModels(factoryProducer = { factory })
    private val navArgs: BookDetailsFragmentArgs by navArgs()

    private val booksLikeAdapter by lazy {
        BooksLikeAdapter().apply {
            likeBookClickListener = BooksLikeAdapter.IOnItemClickListener { book ->

            }
        }
    }
    private val booksDetailAdapter by lazy {
        BooksDetailAdapter().apply {
            detailBookClickListener = BooksDetailAdapter.IOnItemClickListener { book ->

            }
        }
    }

    private val pagerSnapHelper = PagerSnapHelper()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeViewModel()
        setUpBooksRecyclerView()
        setListeners(binding)
    }

    private fun setListeners(binding: FragmentBookDetailsBinding) {
        binding.ivBack.setOnClickListener {
            findNavController().navigateUp()
        }
        binding.btnReadNow.setOnClickListener {

        }
        binding.rvRecentBooks.addOnScrollListener(object: RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    val position = getSnapPositionChange(recyclerView)
                    viewModel.setSelectedBook(position)
                }
            }
        })
    }

    private fun getSnapPositionChange(recyclerView: RecyclerView): Int {
        return pagerSnapHelper.getSnapPosition(recyclerView)
    }

    private fun scrollToPosition(position: Int) {
        binding.rvRecentBooks.smoothScrollToPosition(position)
    }

    private fun observeViewModel() {
        viewModel.booksFlow.collectWithLifecycle(viewLifecycleOwner) {
            booksDetailAdapter.submitList(it.headerBooks) {
                lifecycleScope.launch {
                    delay(SCROLL_POSITION_DELAY)
                    viewModel.scrollToPosition(navArgs.bookId)
                }
            }
            booksLikeAdapter.submitList(it.recommendBooks)
        }
        viewModel.currentBookFlow.collectWithLifecycle(viewLifecycleOwner) {
            initViews(book = it)
        }
        viewModel.scrollFlow.collectWithLifecycle(viewLifecycleOwner) {
            if (it == 0) {
                viewModel.setSelectedBook(it)
            } else {
                scrollToPosition(it)
            }
        }
    }

    private fun initViews(book: BookModel) {
        with(book) {
            binding.tvTitle.text = name
            binding.tvAuthor.text = author
            binding.tvReadersValue.text = views
            binding.tvLikesValue.text = likes
            binding.tvQuotesValue.text = quotes
            binding.tvGenreValue.text = genre
            binding.tvBookDescription.text = summary
        }
    }

    private fun setUpBooksRecyclerView() {
        binding.rvCategoryBooks.adapter = booksLikeAdapter

        val layoutManager = ProminentLayoutManager(requireContext())
        binding.rvRecentBooks.layoutManager = layoutManager
        binding.rvRecentBooks.adapter = booksDetailAdapter
        pagerSnapHelper.attachToRecyclerView(binding.rvRecentBooks)
        val spacing = resources.getDimensionPixelSize(R.dimen.carousel_spacing)
        binding.rvRecentBooks.addItemDecoration(LinearHorizontalSpacingDecoration(spacing))
        binding.rvRecentBooks.addItemDecoration(BoundsOffsetDecoration())
    }

    companion object {
        const val SCROLL_POSITION_DELAY = 100L
    }
}