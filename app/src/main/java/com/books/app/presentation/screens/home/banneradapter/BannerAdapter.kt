package com.books.app.presentation.screens.home.banneradapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.books.app.databinding.ItemBannerBinding
import com.books.app.domain.model.BannerModel
import com.bumptech.glide.Glide

class BannerAdapter :
    ListAdapter<BannerModel, BannerAdapter.OnBoardingViewHolder>(diffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OnBoardingViewHolder {
        return OnBoardingViewHolder(
            ItemBannerBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(vh: OnBoardingViewHolder, position: Int) {
        vh.bind(getItem(position))
    }

    inner class OnBoardingViewHolder(
        val binding: ItemBannerBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: BannerModel) {
            with(binding) {
                Glide.with(binding.root.context)
                    .load(item.cover)
                    .centerCrop()
                    .into(ivSlide)
            }
        }
    }

    companion object {
        val diffCallback = object : DiffUtil.ItemCallback<BannerModel>() {
            override fun areItemsTheSame(
                oldItem: BannerModel,
                newItem: BannerModel
            ): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(
                oldItem: BannerModel,
                newItem: BannerModel
            ): Boolean {
                return oldItem == newItem
            }
        }
    }
}
