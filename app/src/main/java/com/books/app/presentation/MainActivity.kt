package com.books.app.presentation

import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.lifecycle.ViewModelProvider
import com.books.app.databinding.ActivityMainBinding
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

class MainActivity : DaggerAppCompatActivity() {

    @Inject
    lateinit var factory: ViewModelProvider.Factory
    val viewModel: MainViewModel by viewModels(factoryProducer = { factory })

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        fetchConfigs()
    }

    private fun fetchConfigs() {
        viewModel.remoteConfig.fetchAndActivate().addOnCompleteListener(this) {
            if (it.isSuccessful) {
                Log.d(TAG, "get configs successful")
            } else {
                Log.d(TAG, "get configs failed")
            }
        }
    }

    companion object {
        val TAG = MainActivity::class.java.simpleName
    }
}