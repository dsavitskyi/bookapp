package com.books.app.presentation.screens.home

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.viewpager2.widget.ViewPager2
import androidx.viewpager2.widget.ViewPager2.OnPageChangeCallback
import com.books.app.databinding.FragmentHomeBinding
import com.books.app.domain.extentions.collectWithLifecycle
import com.books.app.presentation.ViewBindingFragment
import com.books.app.presentation.screens.home.banneradapter.BannerAdapter
import com.books.app.presentation.screens.home.booksadapter.BooksAdapter
import com.google.android.material.tabs.TabLayoutMediator
import javax.inject.Inject

class HomeFragment: ViewBindingFragment<FragmentHomeBinding>(FragmentHomeBinding::inflate) {

    @Inject
    lateinit var factory: ViewModelProvider.Factory
    val viewModel: HomeViewModel by viewModels(factoryProducer = { factory })

    private val booksAdapter by lazy {
        BooksAdapter().apply {
            booksClickListener = BooksAdapter.IOnItemClickListener { book ->
                findNavController().navigate(HomeFragmentDirections.actionHomeFragmentToBookDetailsFragment(
                    bookId = book.id
                ))
            }
        }
    }

    private val bannerAdapter by lazy { BannerAdapter() }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpBanner(binding)
        setUpBooks(binding)
        observeViewModel()
    }

    override fun onStop() {
        super.onStop()
        viewModel.cancelBannerJob()
    }

    private fun setUpBanner(binding: FragmentHomeBinding) {
        with(binding) {
            vpBanner.apply {
                adapter = bannerAdapter
                TabLayoutMediator(tlBanner, this) { tab, position -> }.attach()
                registerOnPageChangeCallback(object : OnPageChangeCallback() {
                    private var currentState = 0
                    private var currentPosition = 0

                    override fun onPageScrolled(
                        position: Int,
                        positionOffset: Float,
                        positionOffsetPixels: Int
                    ) {
                        if (currentState == ViewPager2.SCROLL_STATE_DRAGGING && currentPosition == position && currentPosition == 0)
                            this@apply.currentItem = bannerAdapter.currentList.size - 1
                        else if (currentState == ViewPager2.SCROLL_STATE_DRAGGING && currentPosition == position
                            && currentPosition == bannerAdapter.currentList.size - 1) this@apply.currentItem = 0
                        super.onPageScrolled(position, positionOffset, positionOffsetPixels)
                    }

                    override fun onPageSelected(position: Int) {
                        currentPosition = position
                        super.onPageSelected(position)
                    }

                    override fun onPageScrollStateChanged(state: Int) {
                        currentState = state
                        super.onPageScrollStateChanged(state)
                        if (state == ViewPager2.SCROLL_STATE_DRAGGING) {
                            viewModel.cancelBannerJob()
                        }
                    }
                })
            }
        }
    }

    private fun setUpBooks(binding: FragmentHomeBinding) {
        binding.rvBooks.adapter = booksAdapter
    }

    private fun observeViewModel() {
        viewModel.booksFlow.collectWithLifecycle(viewLifecycleOwner) {
            bannerAdapter.submitList(it.banners) {
                viewModel.prepareBannerAutoScroll(bannerAdapter.currentList.size)
            }
            booksAdapter.submitList(it.books)
        }
        viewModel.updateBannerFlow.collectWithLifecycle(viewLifecycleOwner) {
            binding.vpBanner.setCurrentItem(it, true)
        }
    }
}