package com.books.app.presentation.screens.home.categoryadapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.books.app.R
import com.books.app.databinding.ItemCurrentCategoryBookBinding
import com.books.app.domain.model.BookModel
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.FitCenter
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade
import com.bumptech.glide.request.RequestOptions

class BooksCategoryAdapter :
    ListAdapter<BookModel, BooksCategoryAdapter.ItemViewHolder>(DIFF_UTIL) {

    var categoryBookClickListener: IOnItemClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        return ItemViewHolder(
            ItemCurrentCategoryBookBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind()
    }

    inner class ItemViewHolder(
        private val itemBinding: ItemCurrentCategoryBookBinding,
    ) : RecyclerView.ViewHolder(itemBinding.root) {

        init {
            itemBinding.root.setOnClickListener {
                categoryBookClickListener?.onCategoryBookClick(getItem(adapterPosition))
            }
        }

        fun bind() {
            val currentItem = getItem(adapterPosition)
            with(itemBinding) {
                ivCategoryTitle.text = currentItem.name
                Glide.with(itemBinding.root.context)
                    .load(currentItem.coverUrl)
                    .apply(RequestOptions().override(500, 600))
                    .transition(withCrossFade())
                    .transform(
                        FitCenter(), RoundedCorners(
                        itemBinding.root.context.resources.getDimensionPixelSize(R.dimen.rounded_corners_radius))
                    )
                    .dontAnimate()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .thumbnail(0.5f)
                    .into(ivBookCover)
            }
        }
    }

    fun interface IOnItemClickListener {
        fun onCategoryBookClick(item: BookModel)
    }

    companion object {
        val DIFF_UTIL = object : DiffUtil.ItemCallback<BookModel>() {

            override fun areItemsTheSame(
                oldItem: BookModel,
                newItem: BookModel,
            ): Boolean =
                oldItem.id == newItem.id

            override fun areContentsTheSame(
                oldItem: BookModel,
                newItem: BookModel,
            ): Boolean =
                oldItem == newItem
        }
    }
}