package com.books.app.presentation.screens.welcome

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.books.app.presentation.ViewBindingFragment
import com.books.app.databinding.FragmentWelcomeBinding
import com.books.app.domain.extentions.collectWithLifecycle
import javax.inject.Inject

class WelcomeFragment: ViewBindingFragment<FragmentWelcomeBinding>(FragmentWelcomeBinding::inflate) {

    @Inject
    lateinit var factory: ViewModelProvider.Factory
    val viewModel: WelcomeViewModel by viewModels(factoryProducer = { factory })

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeViewModel()
    }

    private fun observeViewModel() {
        viewModel.navigationFlow.collectWithLifecycle(viewLifecycleOwner) {
            if (it) findNavController().navigate(WelcomeFragmentDirections.actionWelcomeFragmentToHomeFragment())
        }
    }
}