package com.books.app.presentation.screens.home.booksadapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.books.app.databinding.ItemBooksCategoryBinding
import com.books.app.domain.model.BookModel
import com.books.app.domain.model.SortedBooksModel
import com.books.app.presentation.screens.home.categoryadapter.BooksCategoryAdapter

class BooksAdapter :
    ListAdapter<SortedBooksModel, BooksAdapter.ItemViewHolder>(DIFF_UTIL) {

    var booksClickListener: IOnItemClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        return ItemViewHolder(
            ItemBooksCategoryBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind()
    }

    inner class ItemViewHolder(
        private val itemBinding: ItemBooksCategoryBinding,
    ) : RecyclerView.ViewHolder(itemBinding.root) {

        private val booksCategoryAdapter by lazy {
            BooksCategoryAdapter().apply {
                categoryBookClickListener = BooksCategoryAdapter.IOnItemClickListener { book ->
                    booksClickListener?.onBooksClick(book)
                }
            }
        }

        fun bind() {
            val currentItem = getItem(adapterPosition)
            with(itemBinding) {
                if (currentItem.books.isNotEmpty()) {
                    ivCategoryTitle.text = currentItem.books[0].genre
                }
                rvCategoryBooks.adapter = booksCategoryAdapter
                booksCategoryAdapter.submitList(currentItem.books)
            }
        }
    }

    fun interface IOnItemClickListener {
        fun onBooksClick(item: BookModel)
    }

    companion object {
        val DIFF_UTIL = object : DiffUtil.ItemCallback<SortedBooksModel>() {

            override fun areItemsTheSame(
                oldItem: SortedBooksModel,
                newItem: SortedBooksModel,
            ): Boolean =
                oldItem == newItem

            override fun areContentsTheSame(
                oldItem: SortedBooksModel,
                newItem: SortedBooksModel,
            ): Boolean =
                oldItem == newItem
        }
    }
}