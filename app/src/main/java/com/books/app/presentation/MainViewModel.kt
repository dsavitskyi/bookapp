package com.books.app.presentation

import androidx.lifecycle.ViewModel
import com.books.app.domain.repository.IBooksRepository
import javax.inject.Inject

class MainViewModel @Inject constructor(
    private val booksRepository: IBooksRepository
) : ViewModel() {

    val remoteConfig = booksRepository.activateConfigs()
}