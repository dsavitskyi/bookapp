package com.books.app.di.modules.activity

import androidx.lifecycle.ViewModel
import com.books.app.di.modules.viewmodel.BooksDetailsViewModelModule
import com.books.app.presentation.MainViewModel
import com.books.app.di.modules.viewmodel.HomeViewModelModule
import com.books.app.di.modules.viewmodel.ViewModelKey
import com.books.app.di.modules.viewmodel.WelcomeViewModelModule
import com.books.app.di.scope.FragmentScope
import com.books.app.presentation.screens.details.BookDetailsFragment
import com.books.app.presentation.screens.home.HomeFragment
import com.books.app.presentation.screens.welcome.WelcomeFragment
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
interface MainActivityModule {

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    fun bindMainViewModel(model: MainViewModel): ViewModel

    @FragmentScope
    @ContributesAndroidInjector(modules = [WelcomeViewModelModule::class])
    fun welcomeFragment(): WelcomeFragment

    @FragmentScope
    @ContributesAndroidInjector(modules = [HomeViewModelModule::class])
    fun homeFragment(): HomeFragment

    @FragmentScope
    @ContributesAndroidInjector(modules = [BooksDetailsViewModelModule::class])
    fun bookDetailsFragment(): BookDetailsFragment
}