package com.books.app.di

import android.content.Context
import com.books.app.presentation.App
import com.books.app.di.annotations.ApplicationContext
import com.books.app.di.modules.activity.ActivityBindingModule
import com.books.app.di.modules.activity.MainActivityModule
import com.books.app.di.modules.repository.RepositoryModule
import com.books.app.di.modules.viewmodel.ViewModelModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        ActivityBindingModule::class,
        ViewModelModule::class,
        MainActivityModule::class,
        RepositoryModule::class
    ]
)
interface AppComponent : AndroidInjector<App> {


    @Component.Factory
    interface Factory {

        fun create(@BindsInstance @ApplicationContext context: Context): AppComponent
    }
}