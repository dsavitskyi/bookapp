package com.books.app.di.modules.viewmodel

import androidx.lifecycle.ViewModel
import com.books.app.presentation.screens.home.HomeViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
interface HomeViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(HomeViewModel::class)
    fun bindHomeViewModel(model: HomeViewModel): ViewModel
}