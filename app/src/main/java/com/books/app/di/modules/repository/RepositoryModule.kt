package com.books.app.di.modules.repository

import com.books.app.data.repository.BooksRepository
import com.books.app.domain.repository.IBooksRepository
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
interface RepositoryModule {

    @Binds
    @Singleton
    fun bindActionsRepository(repo: BooksRepository): IBooksRepository

}