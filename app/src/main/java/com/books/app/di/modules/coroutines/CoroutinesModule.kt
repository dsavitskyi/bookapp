package com.books.app.di.modules.coroutines

import com.books.app.domain.coroutine.ICoroutineDispatchers
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import javax.inject.Singleton

@Module
object CoroutinesModule {

    @Provides
    @Singleton
    fun provideDispatchers(): ICoroutineDispatchers {
        return object : ICoroutineDispatchers {

            override val io: CoroutineDispatcher = Dispatchers.IO
            override val processing: CoroutineDispatcher = Dispatchers.Default
            override val ui: CoroutineDispatcher = Dispatchers.Main

        }
    }
}