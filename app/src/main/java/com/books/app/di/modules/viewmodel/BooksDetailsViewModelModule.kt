package com.books.app.di.modules.viewmodel

import androidx.lifecycle.ViewModel
import com.books.app.presentation.screens.details.BookDetailsViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
interface BooksDetailsViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(BookDetailsViewModel::class)
    fun bindBookDetailsViewModel(model: BookDetailsViewModel): ViewModel
}