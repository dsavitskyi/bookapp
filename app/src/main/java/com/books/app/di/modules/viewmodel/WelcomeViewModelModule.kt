package com.books.app.di.modules.viewmodel

import androidx.lifecycle.ViewModel
import com.books.app.presentation.screens.welcome.WelcomeViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
interface WelcomeViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(WelcomeViewModel::class)
    fun bindWelcomeViewModel(model: WelcomeViewModel): ViewModel
}